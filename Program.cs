﻿using System;
using System.IO;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var node1 = new ListNode()
            {
                Data = "Hello,",
            };
            node1.Random = node1;

            var node2 = new ListNode()
            {
                Data = "Saber",
            };

            var node3 = new ListNode()
            {
                Data = "Interactive",
            };

            ListRandom lr = new ListRandom();

            lr.Add(node1,node2,node3);

            node3.Random = lr.GetRandomNode();

            using(FileStream writer = File.Open(@"C:\Users\makmu\Desktop\Кейс\Temp\f.txt", FileMode.Create, FileAccess.Write))
            {
                lr.Serialize(writer);
            }

            var list = new ListRandom();

            using(FileStream reader = File.Open(@"C:\Users\makmu\Desktop\Кейс\Temp\f.txt", FileMode.OpenOrCreate, FileAccess.Read))
            {
                list.Deserialize(reader);
            }

            Console.WriteLine(lr.GetInfo());
            Console.WriteLine(list.GetInfo());
            Console.WriteLine($"Lists equivalence: {list.Equals(lr)}");

            Console.ReadKey();
        }
    }
}
