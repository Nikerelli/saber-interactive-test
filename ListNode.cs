﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace TestApp
{
    public class ListNode
    {
        public ListNode Privious;
        public ListNode Next;
        public ListNode Random;
        public string Data;

        public string GetInfo()
        {
            var nextData = Next != null ? Next.Data : "No next node or data";
            var prevData = Privious != null ? Privious.Data : "No previouse node or data";
            var randData = Random != null ? Random.Data : "No Random node or data";

            var sb = new StringBuilder();
            sb.AppendLine($"Node with data: [{Data}]");
            sb.AppendLine($"Next data: {nextData}");
            sb.AppendLine($"Previouse data: {prevData}");
            sb.AppendLine($"Random data: {randData}");
            sb.AppendLine("\n");

            return sb.ToString();
        }
    }
}
