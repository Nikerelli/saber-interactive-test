﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestApp
{
    public class ListRandom
    {
        public ListNode Head;
        public ListNode Tail;
        public int Count;

        /// <summary>
        /// Contructor
        /// </summary>
        public ListRandom()
        {
            Count = 0;
        }

        /// <summary>
        /// Deserialize <see cref="ListRandom"/> from <see cref="byte[]"/> 
        /// </summary>
        /// <param name="stream"></param>
        public void Deserialize(Stream stream)
        {
            if (stream.CanRead)
            {
                var indexesTable = new List<int>();
                var totalRead = 0;
                try
                {
                    ListNode privElem = null;
                    while (stream.Length != totalRead)
                    {
                        var buffer = new byte[4];
                        var boolBuffer = new byte[1];
                        totalRead += stream.Read(boolBuffer,0,1);

                        var randomExists = BitConverter.ToBoolean(boolBuffer, 0);
                        if (randomExists)
                        {
                            totalRead += stream.Read(buffer, 0, 4);
                            indexesTable.Add(BitConverter.ToInt32(buffer, 0));
                        }
                        else
                            indexesTable.Add(-1);

                        totalRead += stream.Read(buffer, 0, 4);
                        var dataLength = BitConverter.ToInt32(buffer, 0);

                        var dataBytes = new byte[dataLength];
                        totalRead += stream.Read(dataBytes, 0, dataBytes.Length);
                        var data = Encoding.UTF8.GetString(dataBytes);

                        var newNode = new ListNode()
                        {
                            Data = data,
                            Privious = privElem
                        };

                        if (privElem != null)
                            privElem.Next = newNode;

                        privElem = newNode;

                        if (Head == null)
                        {
                            Head = privElem;
                            Tail = privElem;
                        }



                        Count++;
                    }

                    Tail = privElem;

                    var node = Head;
                    foreach (int ind in indexesTable)
                    {
                        node.Random = ind != -1 ? this.GetNodeAt(ind) : null;
                        node = node.Next;
                    }
                }
                catch
                {
                    Console.WriteLine($"Error at {typeof(ListRandom)} while serializing");
                }
            }
            else
                throw new Exception("Stream has no right to read");
        }

        /// <summary>
        /// Serialize <see cref="ListRandom"/> as sequence of <see cref="byte[]"/>
        /// </summary>
        /// <param name="stream"></param>
        public void Serialize(Stream stream)
        {
            if (stream.CanWrite)
            {
                try
                {
                    if (Head != null)
                    {
                        var node = Head;
                        do
                        {
                            var buffer = node.SerializeNode(node.Random != null ? this.IndexOf(node.Random) : -1);
                            stream.Write(buffer, 0, buffer.Length);
                            node = node.Next;
                        } while (node != null);
                    }
                }
                catch
                {
                    Console.WriteLine($"Error at {typeof(ListRandom)} while serializing");
                }
            }
            else
                throw new Exception("Stream has no right to write");
        }

        /// <summary>
        /// Adds element to the list structure
        /// </summary>
        /// <param name="node"></param>
        public void Add(ListNode node)
        {
            if (Tail == null)
            {
                Tail = node;
                Head = node;

                Count++;

                return;
            }

            Tail.Next = node;
            node.Privious = Tail;
            Tail = node;

            Count++;
        }

        /// <summary>
        /// Add a range of nodes to the list structure
        /// </summary>
        /// <param name="nodes"></param>
        public void Add(params ListNode[] nodes)
        {
            foreach (var node in nodes)
            {
                this.Add(node);   
            }
        }

        /// <summary>
        /// Returns a string with information of a list
        /// </summary>
        /// <returns></returns>
        public string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine("-----------------------LIST DATA");

            var node = Head;
            do
            {
                sb.Append(node.GetInfo());
                node = node.Next;
            } while (node != null);

            sb.AppendLine("-----------------------LIST END\n");
            return sb.ToString();
        }

        /// <summary>
        /// Describes the equality of two different lists
        /// </summary>
        /// <param name="rd1"></param>
        /// <returns></returns>
        public bool Equals(ListRandom rd1)
        {
            if (rd1 == null || rd1.Count != Count)
            {
                return false;
            }

            var innerNode = Head;
            var externalNode = rd1.Head;
            do
            {
                if (innerNode.Equals(externalNode))
                    return false;
                innerNode = innerNode.Next;
                externalNode = externalNode.Next;
            } while (innerNode.Next != null);

            return true;
        }



        /// <summary>
        /// Return a random node from List structure
        /// </summary>
        /// <returns></returns>
        public ListNode GetRandomNode()
        {
            var rd = new Random();
            return this.GetNodeAt(rd.Next(0, Count));
        }
    }
}
