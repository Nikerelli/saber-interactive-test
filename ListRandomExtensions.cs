﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text;

namespace TestApp
{
    public static class ListRandomExtensions
    {

        /// <summary>
        /// Returns index of node at the current ListRandom
        /// if there is no such node returns -1
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static int IndexOf(this ListRandom nodes, ListNode node)
        {
            var currentNode = nodes.Head;
            var ctr = 0;

            do
            {
                if (currentNode.Equals(node))
                    return ctr;
                ctr++;
                currentNode = currentNode.Next;
            } while (currentNode != null);

            return -1;
        }

        /// <summary>
        /// Returns the node at the index <param name="index"></param>
        /// if index out of range returns null
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static ListNode GetNodeAt(this ListRandom nodes, int index)
        {
            if (index < 0 || nodes.Count < index + 1)
                return null;

            var closerEdge = new ListNode();
            var distance = 0;

            if (Math.Abs(index - nodes.Count + 2) > index + 1)
            {
                closerEdge = nodes.Head;
                distance = index;

                if (nodes.Count > 1)
                    while (distance != 0)
                    {
                        closerEdge = closerEdge.Next;
                        distance--;
                    }
                else
                    return nodes.Head;
            }
            else
            {
                closerEdge = nodes.Tail;
                distance = nodes.Count - index - 1;

                if (nodes.Count > 1)
                    while (distance != 0)
                    {
                        closerEdge = closerEdge.Privious;
                        distance--;
                    }
                else
                    return nodes.Head;
            }

            return closerEdge;
        }
    }
}
