﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp
{
    public static class ListNodeExtension
    {
        /// <summary>
        /// Serialize node as byte sequense
        /// </summary>
        /// <param name="node"></param>
        /// <param name="randomInd"></param>
        /// <returns></returns>
        public static byte[] SerializeNode(this ListNode node, int randomInd)
        {
            if (node == null)
                return null;

            var dataBytes = Encoding.UTF8.GetBytes(node.Data);
            bool randomExists = false;
            byte[] randomIndex = new byte[4];
            byte[] dataLength = BitConverter.GetBytes(dataBytes.Length);

            if (randomInd != -1)
            {
                randomExists = true;
                randomIndex = BitConverter.GetBytes(randomInd);
            }

            byte[] randomFlag = BitConverter.GetBytes(randomExists);
            byte[] allData = null;
            var allLength = randomFlag.Length + dataLength.Length + dataBytes.Length; //4 - segment length

            if (randomExists)
            {
                allLength += randomIndex.Length;
                allData = new byte[allLength];

                randomFlag.CopyTo(allData, 0);
                randomIndex.CopyTo(allData, 1);
                dataLength.CopyTo(allData, 5);
                dataBytes.CopyTo(allData, 9);
            }
            else
            {
                allData = new byte[allLength];
                randomFlag.CopyTo(allData, 0);
                dataLength.CopyTo(allData, 1);
                dataBytes.CopyTo(allData, 5);
            }

            Console.WriteLine($"Node size: {allData.Length} bytes");
            return allData;
        }
    }
}
